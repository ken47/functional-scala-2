package streams

trait SolutionChecker extends GameDef with Solver with StringParserTerrain {
  /**
   * This method applies a list of moves `ls` to the block at position
   * `startPos`. This can be used to verify if a certain list of moves
   * is a valid solution, i.e. leads to the goal.
   */
  def solve(ls: List[Move]): Block =
    ls.foldLeft(startBlock) {
      case (block, move) => move match {
        case Left => block.left
        case Right => block.right
        case Up => block.up
        case Down => block.down
      }
    }
}

trait Level1 extends SolutionChecker {
  /* terrain for level 1*/

  val level =
    """ooo-------
      |oSoooo----
      |ooooooooo-
      |-ooooooooo
      |-----ooToo
      |------ooo-""".stripMargin

  val optsolution = List(Right, Right, Down, Right, Right, Right, Down)
}

object test {
  val asdf = new Level1 {
    println(startPos.x)
    println(startPos.y)
    val block = new Block(startPos, startPos)
    val initial = Stream((new Block(startPos, startPos), List()))
    
    val neighbors = neighborsWithHistory(initial.head._1, initial.head._2)
  }                                               //> 1
                                                  //| 1
                                                  //| asdf  : streams.Level1{val block: this.Block; val initial: scala.collection
                                                  //| .immutable.Stream[(this.Block, List[Nothing])]; val neighbors: Stream[(this
                                                  //| .Block, List[this.Move])]} = streams.test$$anonfun$main$1$$anon$1@582178a6
  asdf.solution                                   //> Pos(1,1), Pos(1,1)\

}