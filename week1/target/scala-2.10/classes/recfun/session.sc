package recfun
import Main._

object session {
val test = List(1,2)                              //> test  : List[Int] = List(1, 2)

	countChange(4, test);                     //> res0: Int = 3
}