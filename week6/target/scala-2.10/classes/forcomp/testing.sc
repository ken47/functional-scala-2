package forcomp

import forcomp.Anagrams._

object testing {
  def fib(x: Int): Int = x match {
    case 0 => 0
    case 1 => 1
    case _ => fib(x - 1) + fib(x - 2)
  }                                               //> fib: (x: Int)Int
  def fibTrace(x: Int): Int = {
    println("fibtrace(" + x + ")")
    x match {
      case 0 => 0
      case 1 => 1
      case _ => fibTrace(x - 1) + fibTrace(x - 2)
    }
  }                                               //> fibTrace: (x: Int)Int

  def fibCount(x: Int): Int = {
    def helper(x: Int): (Int, Int) = x match {
      case 0 => (1, 0)
      case 1 => (1, 1)
      case _ => {
        val (calls1, result1) = helper(x - 1)
        val (calls2, result2) = helper(x - 2)
        ((1 + calls1 + calls2), result1 + result2)
      }
    }

    val (calls, result) = helper(x)
    println("fibCount(" + x + "): " + calls + " calls")
    result
  }                                               //> fibCount: (x: Int)Int

  class IntFunctionMemoPrecachedLazy(f: Int => Int, val min: Int, val max: Int) extends (Int => Int) {
    lazy val cache = (for (i <- min to max) yield (i, new Lazy(f(i)))).toMap
    def apply(x: Int): Int = cache.get(x) match {
      case Some(lazyObj) => lazyObj.value
      case None => f(x)
    }
  }

  class IntFunctionStream(f: Int => Int, val min: Int, val max: Int) extends (Int => Int) {
    def from(n: Int): Stream[Int] = n #:: from(n + 1)
    val nats = from(0)
    val mapped = nats map f
    def apply(x: Int): Int = mapped.drop(x).head
  }

  class Lazy[T](expr: => T) { lazy val value = expr }

  def fibMemo(x: Int): Int = {
    lazy val fibLazy = new IntFunctionStream(helper, 0, 40)
    def helper(x: Int): Int = {
      println("EVALING FOR " + x)
      x match {
        case 0 => 1
        case 1 => 1
        case _ => fibLazy(x - 1) + fibLazy(x - 2)
      }
    }
    helper(x)
  }                                               //> fibMemo: (x: Int)Int

  fibMemo(30)                                     //> EVALING FOR 30
                                                  //| EVALING FOR 0
                                                  //| EVALING FOR 1
                                                  //| EVALING FOR 2
                                                  //| EVALING FOR 3
                                                  //| EVALING FOR 4
                                                  //| EVALING FOR 5
                                                  //| EVALING FOR 6
                                                  //| EVALING FOR 7
                                                  //| EVALING FOR 8
                                                  //| EVALING FOR 9
                                                  //| EVALING FOR 10
                                                  //| EVALING FOR 11
                                                  //| EVALING FOR 12
                                                  //| EVALING FOR 13
                                                  //| EVALING FOR 14
                                                  //| EVALING FOR 15
                                                  //| EVALING FOR 16
                                                  //| EVALING FOR 17
                                                  //| EVALING FOR 18
                                                  //| EVALING FOR 19
                                                  //| EVALING FOR 20
                                                  //| EVALING FOR 21
                                                  //| EVALING FOR 22
                                                  //| EVALING FOR 23
                                                  //| EVALING FOR 24
                                                  //| EVALING FOR 25
                                                  //| EVALING FOR 26
                                                  //| EVALING FOR 27
                                                  //| EVALING FOR 28
                                                  //| EVALING FOR 29
                                                  //| res0: Int = 1346269
}