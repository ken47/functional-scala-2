package forcomp

import common._

object Anagrams {

  /** A word is simply a `String`. */
  type Word = String

  /** A sentence is a `List` of words. */
  type Sentence = List[Word]

  /**
   * `Occurrences` is a `List` of pairs of characters and positive integers saying
   *  how often the character appears.
   *  This list is sorted alphabetically w.r.t. to the character in each pair.
   *  All characters in the occurrence list are lowercase.
   *
   *  Any list of pairs of lowercase characters and their frequency which is not sorted
   *  is **not** an occurrence list.
   *
   *  Note: If the frequency of some character is zero, then that character should not be
   *  in the list.
   */
  type Occurrences = List[(Char, Int)]

  /**
   * The dictionary is simply a sequence of words.
   *  It is predefined and obtained as a sequence using the utility method `loadDictionary`.
   */
  val dictionary: List[Word] = loadDictionary

  /**
   * Converts the word into its character occurence list.
   *
   *  Note: the uppercase and lowercase version of the character are treated as the
   *  same character, and are represented as a lowercase character in the occurrence list.
   */
  def wordOccurrences(w: Word): Occurrences = {
    w.toLowerCase.toList.groupBy((c: Char) => c).toList
      .map((pair: (Char, List[Char])) => (pair._1, pair._2.length))
      .sortBy((pair: (Char, Int)) => pair._1)
  }

  /** Converts a sentence into its character occurrence list. */
  def sentenceOccurrences(s: Sentence): Occurrences = s match {
    case Nil => List()
    case word :: words =>
      wordOccurrences(s.reduceLeft(_ + _))
  }

  /**
   * The `dictionaryByOccurrences` is a `Map` from different occurrences to a sequence of all
   *  the words that have that occurrence count.
   *  This map serves as an easy way to obtain all the anagrams of a word given its occurrence list.
   *
   *  For example, the word "eat" has the following character occurrence list:
   *
   *     `List(('a', 1), ('e', 1), ('t', 1))`
   *
   *  Incidentally, so do the words "ate" and "tea".
   *
   *  This means that the `dictionaryByOccurrences` map will contain an entry:
   *
   *    List(('a', 1), ('e', 1), ('t', 1)) -> Seq("ate", "eat", "tea")
   *
   */
  lazy val dictionaryByOccurrences: Map[Occurrences, List[Word]] =
    dictionary.groupBy((w: Word) => wordOccurrences(w)).withDefaultValue(List())

  /** Returns all the anagrams of a given word. */
  def wordAnagrams(word: Word): List[Word] = {
    dictionaryByOccurrences(wordOccurrences(word))
  }

  /**
   * Returns the list of all subsets of the occurrence list.
   *  This includes the occurrence itself, i.e. `List(('k', 1), ('o', 1))`
   *  is a subset of `List(('k', 1), ('o', 1))`.
   *  It also include the empty subset `List()`.
   *
   *  Example: the subsets of the occurrence list `List(('a', 2), ('b', 2))` are:
   *
   *    List(
   *      List(),
   *      List(('a', 1)),
   *      List(('a', 2)),
   *      List(('b', 1)),
   *      List(('a', 1), ('b', 1)),
   *      List(('a', 2), ('b', 1)),
   *      List(('b', 2)),
   *      List(('a', 1), ('b', 2)),
   *      List(('a', 2), ('b', 2))
   *    )
   *
   *  Note that the order of the occurrence list subsets does not matter -- the subsets
   *  in the example above could have been displayed in some other order.
   */
  def combinations(occurrences: Occurrences): List[Occurrences] = {
    def helper(occP: Occurrences, occAcc: Occurrences, acc: List[Occurrences]): List[Occurrences] = {
      occP match {
        case Nil => occAcc.sortBy((pair: (Char, Int)) => pair._1) :: acc
        case first :: rest =>
          ((0 to first._2).map(charCount => {
            if (charCount == 0) {
              helper(rest, occAcc, acc)
            } else {
              helper(rest, (first._1, charCount) :: occAcc, acc)
            }
          })).flatten.toList
      }
    }

    helper(occurrences, List(), List())
  }

  //  val cMemo = scala.collection.mutable.Map[Occurrences, List[Occurrences]]()
  //  def comboMemo(occurrences: Occurrences): List[Occurrences] = {
  //    if (!cMemo.contains(occurrences)) {
  //      cMemo.put(occurrences, combinations(occurrences))
  //    }
  //    cMemo(occurrences)
  //  }

  /**
   * Subtracts occurrence list `y` from occurrence list `x`.
   *
   *  The precondition is that the occurrence list `y` is a subset of
   *  the occurrence list `x` -- any character appearing in `y` must
   *  appear in `x`, and its frequency in `y` must be smaller or equal
   *  than its frequency in `x`.
   *
   *  Note: the resulting value is an occurrence - meaning it is sorted
   *  and has no zero-entries.
   */
  def subtract(x: Occurrences, y: Occurrences): Occurrences = {
    val xp = x.toMap;
    val yp = y.toMap

    def helper(original: Map[Char, Int], toSubtract: (Char, Int)): Map[Char, Int] = {
      val (c, i) = toSubtract
      val newVal = original(c) - i

      if (newVal == 0) original - c
      else original.updated(c, newVal)
    }

    (yp foldLeft xp)(helper).toList.sortBy((pair: (Char, Int)) => pair._1)
  }

  /**
   * Returns a list of all anagram sentences of the given sentence.
   *
   *  An anagram of a sentence is formed by taking the occurrences of all the characters of
   *  all the words in the sentence, and producing all possible combinations of words with those characters,
   *  such that the words have to be from the dictionary.
   *
   *  The number of words in the sentence and its anagrams does not have to correspond.
   *  For example, the sentence `List("I", "love", "you")` is an anagram of the sentence `List("You", "olive")`.
   *
   *  Also, two sentences with the same words but in a different order are considered two different anagrams.
   *  For example, sentences `List("You", "olive")` and `List("olive", "you")` are different anagrams of
   *  `List("I", "love", "you")`.
   *
   *  Here is a full example of a sentence `List("Yes", "man")` and its anagrams for our dictionary:
   *
   *    List(
   *      List(en, as, my),
   *      List(en, my, as),
   *      List(man, yes),
   *      List(men, say),
   *      List(as, en, my),
   *      List(as, my, en),
   *      List(sane, my),
   *      List(Sean, my),
   *      List(my, en, as),
   *      List(my, as, en),
   *      List(my, sane),
   *      List(my, Sean),
   *      List(say, men),
   *      List(yes, man)
   *    )
   *
   *  The different sentences do not have to be output in the order shown above - any order is fine as long as
   *  all the anagrams are there. Every returned word has to exist in the dictionary.
   *
   *  Note: in case that the words of the sentence are in the dictionary, then the sentence is the anagram of itself,
   *  so it has to be returned in this list.
   *
   *  Note: There is only one anagram of an empty sentence.
   */
  def sentenceAnagrams(sentence: Sentence): List[Sentence] = {
    var count = 0
    
    def helper(occListP: Occurrences, sentAcc: Sentence, acc: List[Sentence]): List[Sentence] = {
      count += 1
      if (occListP.isEmpty) {
        sentAcc :: acc
      } else {
        combinations(occListP).filter(combo => !combo.isEmpty).flatMap(occListPP => {
          dictionaryByOccurrences(occListPP).filter(occP => !occP.isEmpty).flatMap(word => {
            helper(subtract(occListP, occListPP), word :: sentAcc, acc)
          })
        })
      }
    }

    
    val ret = helper(sentenceOccurrences(sentence), List(), List())
    
    println(count)
    
    ret
  }

  def sentenceAnagramsView(sentence: Sentence): List[Sentence] = {
    var count = 0
    
    def helper(occListP: Occurrences, sentAcc: Sentence, acc: List[Sentence]): List[Sentence] = {
      count += 1
      if (occListP.isEmpty) {
        sentAcc :: acc
      } else {
        combinations(occListP).view.filter(combo => !combo.isEmpty).flatMap(occListPP => {
          dictionaryByOccurrences(occListPP).filter(occP => !occP.isEmpty).flatMap(word => {
            helper(subtract(occListP, occListPP), word :: sentAcc, acc)
          })
        }).toList
      }
    }

    
    val ret = helper(sentenceOccurrences(sentence), List(), List())
    
    println(count)
    
    ret
  }

  def sentenceAnagramsB(sentence: Sentence): List[Sentence] = {
    def helper(occListP: Occurrences, sentAcc: Sentence, acc: List[Sentence]): List[Sentence] = {
      if (occListP.isEmpty) {
        sentAcc :: acc
      } else {
        val listAcc: List[Sentence] = List()

        combinations(occListP).filter(combo => !combo.isEmpty).foldLeft(listAcc)((foldAcc, occListPP) => {
          val listAcc: List[Sentence] = List()

          foldAcc ++ dictionaryByOccurrences(occListPP).filter(occP => !occP.isEmpty).foldLeft(listAcc)((foldAcc, word) => {
            foldAcc ++ helper(subtract(occListP, occListPP), word :: sentAcc, acc)
          })
        })
      }
    }

    helper(sentenceOccurrences(sentence), List(), List())
  }

  type Memo = Map[Occurrences, Set[Sentence]]

  def combineMemoHelper(merged: Memo, toAdd: (Occurrences, Set[Sentence])): Memo = {
    if (merged.contains(toAdd._1)) merged.updated(toAdd._1, merged(toAdd._1) ++ toAdd._2)
    else merged.updated(toAdd._1, toAdd._2)
  }

  def combineMemos(x: Memo, y: Memo): Memo = (x foldLeft y)(combineMemoHelper)

  def sentenceAnagramsOld(sentence: Sentence): List[Sentence] = {
    var hits = 0
    var misses = 0

    def helper(occListP: Occurrences, memo: Memo): (List[Sentence], Memo) = {
      if (occListP.isEmpty) (List(List()), memo)
      else {
        val listAcc: List[Sentence] = List()

        combinations(occListP).foldLeft((listAcc, memo))((acc, occListPP) => {
          if (occListPP.isEmpty) (List(), acc._2)
          else {
            val diff = subtract(occListP, occListPP)

            val (sentences, nextMemo) =
              if (acc._2.contains(diff)) {
                hits += 1
                (acc._2(diff), acc._2)
              } else {
                misses += 1
                val (sents, subMemo) = if (occListPP.isEmpty) (List(), acc._2) else helper(diff, memo)
                val memoP = combineMemos(subMemo, acc._2.updated(diff, sents.toSet))
                //                val memoP = acc._2.updated(diff, sents.toSet)
                (sents, memoP)
              }

            val listAcc: List[Sentence] = List()
            // for all sentences generated by the remainder, use
            // the word as a prefix
            val res = acc._1 ++ dictionaryByOccurrences(occListPP).foldLeft(listAcc)((acc, word) => {
              acc ++ sentences.map(sentence => {
                word :: sentence
              })
            })

            (res, nextMemo)
          }
        })
      }
    }

    val memo: Memo = Map()
    val res = helper(sentenceOccurrences(sentence), memo)

    println("HITS " + hits)
    println("MISSES " + misses)

    res._1
  }

  /**
   * purely functional memoization!
   *
   * "concatenating" the map like this is based on the permise that the helper
   * will always return the same result for any given Occurrences
   */
  //  def sentenceAnagramsMemo(sentence: Sentence): List[Sentence] = {
  //    def memoize(occListP: Occurrences, sentAcc: Sentence, acc: Set[Sentence], memo: Memo, originalOcc: Occurrences): (Set[Sentence], Memo) = {
  //      
  //	    def helper(occListP: Occurrences, sentAcc: Sentence, acc: Set[Sentence], memo: Memo): (Set[Sentence], Memo) = {
  //	      // this gatekeeping is wrong because it may prevent the full list of anagrams
  //	      // from being constructed. test with "Sean" to see. 
  //	      // TODO: try to figure out a way to leverage a partial list of anagrams,
  //	      // or a way to only use a FULL list of anagrams
  //
  //        if (occListP.isEmpty) {
  //          (acc + sentAcc, memo)
  //        } else {
  //          val listAcc: Set[Sentence] = Set()
  //          val mapAcc: Memo = memo
  //
  //          combinations(occListP).filter(combo => !combo.isEmpty).foldLeft((listAcc, mapAcc))((foldAcc, occListPP) => {
  //            val listAcc: Set[Sentence] = Set()
  //            val mapAcc: Memo = foldAcc._2
  //
  //            val pr = dictionaryByOccurrences(occListPP).filter(occP => !occP.isEmpty).foldLeft((listAcc, mapAcc))((foldAcc, word) => {
  //              val nextOcc = subtract(occListP, occListPP)
  //
  //              val pr = helper(nextOcc, word :: sentAcc, acc, foldAcc._2)
  //              
  //              memo.updated(nextOcc, sentenceAnagramsMemo(nextOcc))
  //              
  //              (foldAcc._1 ++ pr._1, foldAcc._2 ++ pr._2)
  //            })
  //            (foldAcc._1 ++ pr._1, foldAcc._2 ++ pr._2)
  //          })
  //        }
  //	    }
  //	    
  //	    helper(originalOcc, List(), Set(), memo)
  //    }
  //    
  //    val memo: Memo = Map()
  //    val originalOcc = sentenceOccurrences(sentence)
  //    val res = memoize(originalOcc, List(), Set(), memo, originalOcc)
  ////    println(res._2)
  //    res._1.toList
  //  }

  /**
   * this is based on a crappy algorithm that just happens to have memoization
   * strapped on top
   */
  def sentenceAnagramsMemoB(sentence: Sentence): List[Sentence] = {
    val sMemo = scala.collection.mutable.Map[Occurrences, List[Sentence]]()

    def helperMemo(occListP: Occurrences): List[Sentence] = {
      def helper(occListP: Occurrences): List[Sentence] = {
        if (occListP.isEmpty) List(List())
        else {
          val res = combinations(occListP).map(occListPP => {
            if (occListPP.isEmpty) List()
            else {
              val diff = subtract(occListP, occListPP)
              val sents = if (occListPP.isEmpty) List() else helperMemo(diff)

              if (sents.isEmpty) sents
              else {
                // for all sentences generated by the remainder, use
                // the word as a prefix
                dictionaryByOccurrences(occListPP).flatMap(word => {
                  val res = sents.map(sent => {
                    word :: sent
                  })

                  res
                })
              }
            }
          }).flatten

          res
        }
      }

      if (!sMemo.contains(occListP)) sMemo.put(occListP, helper(occListP))
      sMemo(occListP)
    }

    val occList = sentenceOccurrences(sentence)
    helperMemo(occList)
  }
}
